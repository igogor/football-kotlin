package com.footballapp.sportsonline

import android.content.Context
import android.content.Intent
import android.os.Bundle

import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment

import com.footballapp.sportsonline.fragments.SecondFragment
import kotlinx.android.synthetic.main.app_bar_main.*


class SecondActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        val postId = intent.getIntExtra(EXTRA, 0)
        return SecondFragment.newInstance(postId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    companion object {
        private val EXTRA = SecondActivity::class.java.name + " extraData"

        fun newIntent(context: Context, postId: Int): Intent {
            val intent = Intent(context, SecondActivity::class.java)
            intent.putExtra(EXTRA, postId)
            return intent
        }
    }

}
