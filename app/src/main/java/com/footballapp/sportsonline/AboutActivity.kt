package com.footballapp.sportsonline

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.footballapp.sportsonline.fragments.AboutFragment
import kotlinx.android.synthetic.main.app_bar_main.*

class AboutActivity: SingleFragmentActivity() {
    override fun createFragment(): Fragment {
        return AboutFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
}