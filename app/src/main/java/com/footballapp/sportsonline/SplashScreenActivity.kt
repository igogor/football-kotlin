package com.footballapp.sportsonline

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.footballapp.sportsonline.helpers.NetworkHelper
import com.footballapp.sportsonline.viewmodels.PostsViewModelFactory
import com.footballapp.sportsonline.viewmodels.PostsViewModelPaging
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        load()
    }

    private fun load() {
        progress.visibility = View.VISIBLE
        if (NetworkHelper.isOnline(this)) {

            val factory = PostsViewModelFactory(this.application)
            ViewModelProviders.of(this, factory).get(PostsViewModelPaging::class.java).postPagedList
        }
        Handler().postDelayed({
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 4000)
    }

}
