package com.footballapp.sportsonline.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class PostsViewModelFactory(private val context: Application): ViewModelProvider.AndroidViewModelFactory(context) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostsViewModelPaging::class.java)) {
            val key = "PostsViewModelPaging"
            return if (hashViewModel.containsKey(key)) {
                getViewModel(key) as T
            } else {
                addViewModel(key, PostsViewModelPaging(context) )
                getViewModel(key) as T
            }
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

    companion object {
        private val hashViewModel = HashMap<String, ViewModel>()

        private fun addViewModel(key: String, viewModel: ViewModel) {
            hashViewModel[key] = viewModel
        }

        private fun  getViewModel(key: String): ViewModel? {
            return hashViewModel[key]
        }
    }
}