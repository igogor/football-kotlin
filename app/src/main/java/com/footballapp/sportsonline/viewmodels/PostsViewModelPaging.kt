package com.footballapp.sportsonline.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.footballapp.sportsonline.adapters.pagingLibrary.PostDataSourceFactory
import com.footballapp.sportsonline.adapters.pagingLibrary.PostsDataSource
import com.footballapp.sportsonline.dto.NetworkState
import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.disposables.CompositeDisposable

class PostsViewModelPaging(val context: Application) : AndroidViewModel(context) {
    private val compositeDisposable = CompositeDisposable()
    var postPagedList: LiveData<PagedList<Post>>
        private set
    private var liveDataSource: LiveData<PostsDataSource>
    private val itemDataSourceFactory: PostDataSourceFactory

    init {
        itemDataSourceFactory = PostDataSourceFactory(compositeDisposable)
        liveDataSource = itemDataSourceFactory.postLiveDataSource

        val config = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(5)
                .setPageSize(5)
                .setPrefetchDistance(3)
                .build()

        postPagedList = LivePagedListBuilder(itemDataSourceFactory, config).build()
    }

    fun retry() {
        itemDataSourceFactory.postLiveDataSource.value!!.retry()
    }

    fun refresh() {
        itemDataSourceFactory.postLiveDataSource.value!!.invalidate()
    }

    fun networkState(): LiveData<NetworkState>? = Transformations
            .switchMap(itemDataSourceFactory.postLiveDataSource) { it.networkState() }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}
