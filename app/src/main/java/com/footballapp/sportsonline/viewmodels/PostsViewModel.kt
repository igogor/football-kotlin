package com.footballapp.sportsonline.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.footballapp.sportsonline.repository.NetRepository

import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.repository.DBRepository
import com.footballapp.sportsonline.repository.PostRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class PostsViewModel(val context: Application) : AndroidViewModel(context) {
    private var posts: MutableLiveData<List<Post>> = MutableLiveData()
    private var repository: PostRepository? = null
    private val compositeDisposable = CompositeDisposable()
    private val subject: BehaviorSubject<List<Post>> = BehaviorSubject.createDefault(arrayListOf())

    init {
        repository = PostRepository.getInstance(context)
    }

    fun subscribePosts(page: Int = 0, count: Int = 0) {
        compositeDisposable.clear()
        //val share = subject.share()
        compositeDisposable.add(subject.subscribe(
                { posts.value = it },
                { println(it.printStackTrace()) }
        ))
        compositeDisposable.add(repository!!.getPosts(page, count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {subject.onNext(it)},
                        { println(it.printStackTrace()) }
                ))
    }

    fun getPosts(): MutableLiveData<List<Post>> = posts

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        repository = null
    }

}
