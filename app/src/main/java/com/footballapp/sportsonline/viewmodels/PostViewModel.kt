package com.footballapp.sportsonline.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.repository.PostRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.ResourceMaybeObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class PostViewModel(val context: Application) : AndroidViewModel(context) {
    private var post: MutableLiveData<Post> = MutableLiveData()
    private var repository: PostRepository? = null
    //private val disposable: Disposable
   // private val subject: BehaviorSubject<Post> = BehaviorSubject.create()

    init {
        repository = PostRepository.getInstance(context)
        /*disposable = subject.subscribe {
            post.value = it
        }*/
    }

    fun getPost(): MutableLiveData<Post> {
        return post
    }

    fun loadPost(id: Int) {
        repository!!.getPost(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : ResourceMaybeObserver<Post>() {
                    override fun onSuccess(t: Post) {
                        //subject.onNext(t)
                        post.value = t
                        dispose()
                    }

                    override fun onComplete() {

                    }

                    override fun onError(e: Throwable) {
                        println(e.printStackTrace())
                    }
                })
    }

    override fun onCleared() {
        super.onCleared()
        //disposable.dispose()
        repository = null
    }
}