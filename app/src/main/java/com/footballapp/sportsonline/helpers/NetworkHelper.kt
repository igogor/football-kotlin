package com.footballapp.sportsonline.helpers

import android.content.Context
import android.net.ConnectivityManager

class NetworkHelper {
    companion object {
        @JvmStatic
        fun isOnline(context: Context): Boolean {
            val cm: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo

            return networkInfo != null &&
                    networkInfo.isConnected
        }
    }
}