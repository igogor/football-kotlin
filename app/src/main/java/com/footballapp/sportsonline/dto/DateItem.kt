package com.footballapp.sportsonline.dto

import java.util.Calendar

class DateItem(val date: Calendar, var isChecked: Boolean) : Comparable<DateItem> {

    val day: String
        get() {
            val day = date.get(Calendar.DATE)
            return if (day < 10) {
                "0 $day"
            } else day.toString()
        }

    val month: String
        get() = (date.get(Calendar.MONTH) + 1).toString()

    override fun compareTo(other: DateItem): Int {
        return when {
            this.date == other.date -> 0
            date.after(other.date) -> -1
            else -> 1
        }
    }


}
