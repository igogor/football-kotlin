package com.footballapp.sportsonline.dto.net

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(foreignKeys = [ForeignKey(entity = Post::class,
                parentColumns = ["id"],
                childColumns = ["post_id"],
        onDelete = ForeignKey.CASCADE)],
        indices = [Index("post_id")])
data class Category (
    @SerializedName("id")
    @Expose
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("slug")
    @Expose
    @ColumnInfo(name = "slug")
    var slug: String? = null,
    @SerializedName("title")
    @Expose
    @ColumnInfo(name = "title")
    var title: String? = null,
    @SerializedName("description")
    @Expose
    @ColumnInfo(name = "description")
    var description: String? = null,
    @SerializedName("parent")
    @Expose
    @ColumnInfo(name = "parent")
    var parent: Int = 0,
    @SerializedName("post_count")
    @Expose
    @ColumnInfo(name = "post_count")
    var postCount: Int = 0,
    @ColumnInfo(name = "post_id")
    var postId: Int = 0
)