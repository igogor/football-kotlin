package com.footballapp.sportsonline.dto.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResultPost {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("post")
    @Expose
    var post: Post? = null
    @SerializedName("previous_url")
    @Expose
    var previousUrl: String? = null
    /*@SerializedName("next_url")
    @Expose
    var nextUrl: String? = null*/

    constructor() {}

    constructor(status: String, post: Post, previousUrl: String/*, nextUrl: String*/) {
        this.status = status
        this.post = post
        this.previousUrl = previousUrl
        //this.nextUrl = nextUrl
    }
}
