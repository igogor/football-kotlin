package com.footballapp.sportsonline.dto.net

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "posts")
data class Post(
    @SerializedName("id")
    @Expose
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("type")
    @Expose
    @ColumnInfo(name = "type")
    var type: String? = null,
    @SerializedName("slug")
    @Expose
    @ColumnInfo(name = "slug")
    var slug: String? = null,
    @SerializedName("url")
    @Expose
    @ColumnInfo(name = "url")
    var url: String? = null,
    @SerializedName("status")
    @Expose
    @ColumnInfo(name = "status")
    var status: String? = null,
    @SerializedName("title")
    @Expose
    @ColumnInfo(name = "title")
    var title: String? = null,
    @SerializedName("title_plain")
    @Expose
    @ColumnInfo(name = "title_plain")
    var titlePlain: String? = null,
    @SerializedName("content")
    @Expose
    @ColumnInfo(name = "content")
    var content: String? = null,
    @SerializedName("excerpt")
    @Expose
    @ColumnInfo(name = "excerpt")
    var excerpt: String? = null,
    @SerializedName("date")
    @Expose
    @ColumnInfo(name = "date")
    var date: String? = null,
    @SerializedName("modified")
    @Expose
    @ColumnInfo(name = "modified")
    var modified: String? = null,
   /* @SerializedName("categories")
    @Expose
    //@ColumnInfo(name = "categories")
    @Relation(parentColumn = "id", entityColumn = "post_id")
    var categories: List<Category> = ArrayList(),*/
/*    @SerializedName("tags")
    @Expose
    @ColumnInfo(name = "tags")
    var tags: List<Any> = ArrayList(),*/
    @SerializedName("author")
    @Expose
    //@Embedded
    @Ignore
    var author: Author? = null,
    /*@SerializedName("comments")
    @Expose
    @ColumnInfo(name = "comments")
    var comments: List<Any> = ArrayList(),
    @SerializedName("attachments")
    @Expose
    @ColumnInfo(name = "attachments")
    var attachments: List<Any> = ArrayList(),*/
    @SerializedName("comment_count")
    @Expose
    @ColumnInfo(name = "comment_count")
    var commentCount: Int = 0,
    @SerializedName("comment_status")
    @Expose
    @ColumnInfo(name = "comment_status")
    var commentStatus: String? = null,
    /*
    @SerializedName("custom_fields")
    @Expose
    private CustomFields customFields;*/

    @SerializedName("thumbnail_size")
    @Expose
    @ColumnInfo(name = "thumbnail_size")
    var thumbnailSize: String? = null,
    @SerializedName("thumbnail_images")
    @Expose
    @Embedded
    var thumbnailImages: ThumbnailImages? = null)