package com.footballapp.sportsonline.dto.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class ResultPosts {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("count")
    @Expose
    var count: Int = 0
    @SerializedName("count_total")
    @Expose
    var countTotal: Int = 0
    @SerializedName("pages")
    @Expose
    var pages: Int = 0
    @SerializedName("posts")
    @Expose
    var posts: ArrayList<Post> = ArrayList()

    constructor() {}

    constructor(status: String, count: Int, countTotal: Int, pages: Int, posts: ArrayList<Post>) {
        this.status = status
        this.count = count
        this.countTotal = countTotal
        this.pages = pages
        this.posts = posts
    }
}
