package com.footballapp.sportsonline.dto.net

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "authors",
        foreignKeys = [ForeignKey(
                entity = Post::class,
                parentColumns = ["id"],
                childColumns = ["post_id"],
                onDelete = ForeignKey.CASCADE)],
        indices = [Index("post_id")])
data class Author (
    @SerializedName("id")
    @Expose
    @ColumnInfo(name = "a_id")
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("slug")
    @Expose
    @ColumnInfo(name = "a_slug")
    var slug: String? = null,
    @SerializedName("name")
    @Expose
    @ColumnInfo(name = "name")
    var name: String? = null,
    @SerializedName("first_name")
    @Expose
    @ColumnInfo(name = "first_name")
    var firstName: String? = null,
    @SerializedName("last_name")
    @Expose
    @ColumnInfo(name = "last_name")
    var lastName: String? = null,
    @SerializedName("nickname")
    @Expose
    @ColumnInfo(name = "nick_name")
    var nickname: String? = null,
    @SerializedName("url")
    @Expose
    @ColumnInfo(name = "a_url")
    var url: String? = null,
    @SerializedName("description")
    @Expose
    @ColumnInfo(name = "description")
    var description: String? = null,
    @ColumnInfo(name = "post_id")
    var postId: Int = 0
)
