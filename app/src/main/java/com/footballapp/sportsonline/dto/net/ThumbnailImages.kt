package com.footballapp.sportsonline.dto.net

import androidx.room.ColumnInfo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ThumbnailImages(
        @Expose
        var thumbId: Int = 0,
        @SerializedName("full")
        @Expose
        @ColumnInfo(name = "full")
        var full: Image? = null,
        @SerializedName("thumbnail")
        @Expose
        @ColumnInfo(name = "thumbnail")
        var thumbnail: Image? = null,
        @SerializedName("medium")
        @Expose
        @ColumnInfo(name = "medium")
        var medium: Image? = null,
        @SerializedName("medium_large")
        @Expose
        @ColumnInfo(name = "medium_large")
        var mediumLarge: Image? = null,
        @SerializedName("large")
        @Expose
        @ColumnInfo(name = "large")
        var large: Image? = null,
        @SerializedName("balanced-blog-single")
        @Expose
        @ColumnInfo(name = "balancedBlogSingle")
        var balancedBlogSingle: Image? = null
)