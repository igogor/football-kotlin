package com.footballapp.sportsonline.dto

enum class Status {
    LOADING,
    COMPLETE,
    ERROR
}

data class NetworkState private constructor(
        val status: Status,
        val message: String? = null) {

    companion object {
        val LOADING = NetworkState(Status.LOADING)
        val LOADED = NetworkState(Status.COMPLETE)
        fun error(msg: String?) = NetworkState(Status.ERROR, msg)
    }
}