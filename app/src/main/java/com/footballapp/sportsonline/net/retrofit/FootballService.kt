package com.footballapp.sportsonline.net.retrofit

import com.footballapp.sportsonline.dto.net.ResultPost
import com.footballapp.sportsonline.dto.net.ResultPosts
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface FootballService {
    @Headers("Cache-Control: max-age=0", "User-Agent: Wordpress")
    @GET("?json=get_posts&exclude=content,categories,tags,comments,custom_fields")
    fun getPostByPage(@Query("page") page: Int, @Query("count") count: Int): Single<ResultPosts>

    @Headers("Cache-Control: max-age=0", "User-Agent: Wordpress")
    @GET("?json=get_post")
    fun getPostDetialsById(@Query("id") id: Int): Maybe<ResultPost>
}