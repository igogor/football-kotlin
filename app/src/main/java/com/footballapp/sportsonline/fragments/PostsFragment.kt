package com.footballapp.sportsonline.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.sportsonline.R
import com.footballapp.sportsonline.adapters.pagingLibrary.PostPagingAdapter
import com.footballapp.sportsonline.databinding.PostsFragmentBinding
import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.helpers.NetworkHelper
import com.footballapp.sportsonline.viewmodels.PostsViewModelFactory
import com.footballapp.sportsonline.viewmodels.PostsViewModelPaging

class PostsFragment : Fragment() {
    private lateinit var postsViewModel: PostsViewModelPaging
    private var callbackListener: CallbackListener? = null
    private lateinit var adapter: PostPagingAdapter
    private lateinit var observer: Observer<PagedList<Post>>
    private lateinit var binding: PostsFragmentBinding

    companion object {
        fun newInstance(): PostsFragment = PostsFragment()
    }

    interface CallbackListener {
        fun onItemClick(postId: Int)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CallbackListener)
            callbackListener = context
        else
            throw ClassCastException("$context must implement CallbackListener")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.posts_fragment, container, false)
        observer = Observer {
            cancelRefresh()
            adapter.submitList(it)
            updateAdapter()
        }
        val factory = PostsViewModelFactory(activity!!.application)
        postsViewModel = ViewModelProviders.of(activity!!, factory).get(PostsViewModelPaging::class.java)

        postsViewModel.postPagedList.observe(viewLifecycleOwner, observer)
        postsViewModel.networkState()?.observe(viewLifecycleOwner, Observer {
            binding.networkState = it
        })

        binding.apply {
            list.layoutManager = LinearLayoutManager(activity)
            list.setHasFixedSize(true)

            adapter = PostPagingAdapter(callbackListener!!)
            list.adapter = adapter

            refreshLayout.setOnRefreshListener {
                if (NetworkHelper.isOnline(context!!)) {
                    loadData()
                    updateAdapter()
                } else
                    updateAdapter()
                cancelRefresh()
            }

            errorLayout.refreshButton.setOnClickListener {
                postsViewModel.retry()
            }
        }

        return binding.root
    }

    /*override fun onResume() {
        super.onResume()
        if (NetworkHelper.isOnline(activity!!))
            postsViewModel.getPosts()
    }*/

    override fun onDestroy() {
        super.onDestroy()
        postsViewModel.postPagedList.removeObserver(observer)
        callbackListener = null
    }

    private fun updateAdapter() {
        adapter.notifyDataSetChanged()
    }

    private fun loadData() {
        postsViewModel.refresh()
    }

    private fun cancelRefresh() {
        binding.refreshLayout.isRefreshing = false
    }

    fun sortPosts(byNews: Boolean) {
        /*if (byNews) {
            //adapter.currentList?.sortBy { it.id }
            Collections.sort(postsList) { o1, o2 ->
                val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
                val sdf = SimpleDateFormat(DATE_FORMAT)
                val cal1 = Calendar.getInstance()
                val cal2 = Calendar.getInstance()
                try {
                    cal1.time = sdf.parse(o1.date!!)
                    cal2.time = sdf.parse(o2.date!!)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

                when {
                    cal1.before(cal2) -> -1
                    cal1.after(cal2) -> 0
                    else -> 1
                }
            }
        } else {
            Collections.sort(postsList) { o1, o2 ->
                when {
                    o1.id > o2.id -> -1
                    o1.id < o2.id -> 1
                    else -> 0
                }
            }*/
/*} } else {
    adapter.currentList?.sortByDescending { it.id }

}*/
        //adapter.notifyItemRangeChanged(0, postsList.size)
    }
}