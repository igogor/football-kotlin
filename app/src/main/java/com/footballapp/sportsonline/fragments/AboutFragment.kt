package com.footballapp.sportsonline.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.footballapp.sportsonline.R
import com.footballapp.sportsonline.databinding.ActivityAboutBinding

class AboutFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<ActivityAboutBinding>(inflater, R.layout.activity_about, container, false)
        var version = activity!!.packageManager.getPackageInfo(activity!!.packageName, 0).versionName
        binding.version = String.format("App version %s", version)
        return binding.root
    }
}