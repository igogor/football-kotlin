package com.footballapp.sportsonline.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.footballapp.sportsonline.R
import com.footballapp.sportsonline.databinding.ActivitySecondBinding
import com.footballapp.sportsonline.viewmodels.PostViewModel
import com.squareup.picasso.Picasso

class SecondFragment: Fragment() {
    companion object {
        private val DATA = SecondFragment::class.java.name + "currentPost"

        @JvmStatic
        fun newInstance(postId: Int): SecondFragment {
            val fragment = SecondFragment()
            val args = Bundle()
            args.putInt(DATA, postId)
            fragment.arguments = args
            return fragment
        }

   /*     @JvmStatic
        @BindingAdapter("app:imageUrl")
        fun loadImage(view: ImageView, url: String) {
            Picasso.get().load(url).into(view)
        }*/
    }

    private var postId: Int = 0
    private lateinit var postViewModel: PostViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            postId = it.getInt(DATA)
        }
        postViewModel = ViewModelProviders.of(this).get(PostViewModel::class.java)
        postViewModel.loadPost(postId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: ActivitySecondBinding = DataBindingUtil.inflate(inflater, R.layout.activity_second, container, false)
        binding.lifecycleOwner = this
        postViewModel.getPost().observe(this, Observer {
            binding.post = it
        })
        return binding.root
    }
}