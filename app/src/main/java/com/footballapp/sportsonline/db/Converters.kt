package com.footballapp.sportsonline.db

import androidx.room.TypeConverter
import com.footballapp.sportsonline.dto.net.Image

class Converters {
    @TypeConverter
    fun fromImage(image: Image?): String? {
        if (image == null) {
            return null
        }
        return StringBuilder()
                .append(image.url)
                .append(",")
                .append(image.width.toString())
                .append(",")
                .append(image.height.toString())
                .toString()
    }

    @TypeConverter
    fun toImage(string: String?): Image? {
        if (string == null) return null
        val array = string.split(",")
        return Image(array[0], array[1].toInt(), array[2].toInt())
    }
}