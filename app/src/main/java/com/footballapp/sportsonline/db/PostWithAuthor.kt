package com.footballapp.sportsonline.db

import androidx.room.Embedded
import com.footballapp.sportsonline.dto.net.Author
import com.footballapp.sportsonline.dto.net.Post

data class PostWithAuthor (
    @Embedded
    var post: Post? = null,

    @Embedded
    var author: Author? = null
)