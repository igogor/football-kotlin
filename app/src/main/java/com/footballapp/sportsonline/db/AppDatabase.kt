package com.footballapp.sportsonline.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.footballapp.sportsonline.dto.net.*

@Database(entities = [Author::class, Post::class /*Category::class,*/ ],
        version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase:RoomDatabase() {
    abstract fun postDao(): PostDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context, memoryOnly:Boolean): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = if (!memoryOnly)
                        Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "cool_football")
                                .build()
                    else
                        Room.inMemoryDatabaseBuilder(context.applicationContext, AppDatabase::class.java)
                                .build()
                }
            }
            return INSTANCE!!
        }
    }
}