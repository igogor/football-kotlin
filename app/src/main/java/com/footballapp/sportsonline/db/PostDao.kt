package com.footballapp.sportsonline.db

import androidx.room.*
import com.footballapp.sportsonline.dto.net.Author
import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface PostDao {
    @Query("SELECT * FROM posts")
    fun getAll():Single<List<Post>>

    @Query("SELECT * FROM posts WHERE id IN (:id)")
    fun getPostById(id:Int):Maybe<Post>

    @Insert
    fun insertAll(vararg posts: Post)

    @Delete
    fun deletePost(post: Post)

    @Transaction
    @Query("SELECT posts.*, authors.* FROM posts INNER JOIN authors ON posts.id = authors.post_id")
    fun getPostWithAuthor(): List<PostWithAuthor>

    //Author
    @Query("SELECT * FROM authors")
    fun getAllAuthors():Single<List<Author>>

    @Query("SELECT * FROM authors WHERE post_id = :id")
    fun getAuthorByPostId(id:Int):Maybe<Author>

    @Query("SELECT * FROM authors WHERE a_id = :id")
    fun getAuthorById(id:Int):Maybe<Author>

    @Insert
    fun insertAllAuthors(vararg authors: Author)

    @Delete
    fun deleteAuthor(author: Author)
}