package com.footballapp.sportsonline.adapters.pagingLibrary

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.AdapterListUpdateCallback
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.sportsonline.R
import com.footballapp.sportsonline.databinding.PostListItemBinding
import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.fragments.PostsFragment

class PostPagingAdapter constructor(private val callback: PostsFragment.CallbackListener) : PagedListAdapter<Post, PostPagingAdapter.MatchHolder>(POST_COMPARATOR) {
    private lateinit var currentPost:Post

    companion object {
        private val POST_COMPARATOR = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem == newItem
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<PostListItemBinding>(layoutInflater, R.layout.post_list_item, parent, false)
        return MatchHolder(binding.root)
    }

    override fun onBindViewHolder(holder: MatchHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<PostListItemBinding>(holder.itemView)
        currentPost = getItem(position)!!
        binding?.item = currentPost
        binding?.clickHandler = ClickHandler()
    }

    inner class ClickHandler {
        fun onClick(id: Int) {
            callback.onItemClick(id)
        }
    }

    inner class MatchHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView)
}