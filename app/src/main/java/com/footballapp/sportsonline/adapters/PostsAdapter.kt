package com.footballapp.sportsonline.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.sportsonline.R
import com.footballapp.sportsonline.databinding.PostListItemBinding
import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.fragments.PostsFragment

class PostsAdapter(val context: Context,
                   val listener: PostsFragment.CallbackListener)
    : ListAdapter<Post, PostsAdapter.MatchHolder>(diff_callback) {

    companion object {
        private val diff_callback = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }

    inner class ClickHandler {
        fun onClick(id: Int) {
            listener.onItemClick(id)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding = DataBindingUtil.inflate<PostListItemBinding>(layoutInflater, R.layout.post_list_item, parent, false);
        //binding.clickHandler = ClickHandler()
        return MatchHolder(binding.root)
    }

    override fun onBindViewHolder(holder: MatchHolder, position: Int) {
        val binding = DataBindingUtil.getBinding<PostListItemBinding>(holder.itemView)
        binding?.item = getItem(position)
    }

    inner class MatchHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView)
}