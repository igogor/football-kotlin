package com.footballapp.sportsonline.adapters.pagingLibrary

import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import com.footballapp.sportsonline.dto.NetworkState
import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.repository.NetRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class PostsDataSource(private val disposable: CompositeDisposable) : PositionalDataSource<Post>() {
    private val repository: NetRepository by lazy {
        NetRepository.INSTANCE
    }

    private val networkState = MutableLiveData<NetworkState>()
    private var retryCompletable: Completable? = null

    fun retry() {
        retryCompletable?.let {
            disposable.add(it
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe())
        }
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Post>) {
        networkState.postValue(NetworkState.LOADING)

        disposable.add(repository.getPosts(params.startPosition, params.loadSize)
                .subscribe(
                        {
                            networkState.postValue(NetworkState.LOADED)
                            callback.onResult(it)
                            setRetry(null)
                        },
                        {
                            networkState.postValue(NetworkState.error(it.message))
                            setRetry(Action { loadRange(params, callback) })
                        })
        )
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Post>) {
        networkState.postValue(NetworkState.LOADING)
        repository.currentPage = 1
        disposable.add(repository.getPosts(params.requestedStartPosition, params.requestedLoadSize)
                .subscribe(
                        {
                            networkState.postValue(NetworkState.LOADED)
                            callback.onResult(it, 0)
                            setRetry(null)
                        },
                        {
                            networkState.postValue(NetworkState.error(it.message))
                            setRetry(Action { loadInitial(params, callback) })
                        })
        )
    }

    private fun setRetry(action: Action?) {
        if (action == null) {
            retryCompletable = null
        } else {
            retryCompletable = Completable.fromAction(action)
        }
    }

    fun networkState(): MutableLiveData<NetworkState> = networkState
}
