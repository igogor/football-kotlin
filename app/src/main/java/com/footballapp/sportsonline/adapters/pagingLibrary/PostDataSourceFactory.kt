package com.footballapp.sportsonline.adapters.pagingLibrary

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.disposables.CompositeDisposable

class PostDataSourceFactory(private val disposable: CompositeDisposable): DataSource.Factory<Int, Post>()  {
    val postLiveDataSource = MutableLiveData<PostsDataSource>()

    override fun create(): DataSource<Int, Post> {
        val postsDataSource = PostsDataSource(disposable)
        postLiveDataSource.postValue(postsDataSource)
        return postsDataSource
    }
}