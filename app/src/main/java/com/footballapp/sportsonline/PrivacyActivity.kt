package com.footballapp.sportsonline

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.footballapp.sportsonline.fragments.PrivacyFragment

class PrivacyActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return PrivacyFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
