package com.footballapp.sportsonline

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem

import com.footballapp.sportsonline.fragments.PostsFragment
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.footballapp.sportsonline.fragments.SecondFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main_twopanel.*

class MainActivity:
        SingleFragmentActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        PostsFragment.CallbackListener {

    private var currentIndex = 0
    private lateinit var postsFragment: PostsFragment

    override fun createFragment(): Fragment {
        postsFragment = PostsFragment.newInstance()
        return postsFragment
    }

    override fun onItemClick(postId: Int) {
        if (detail_fragment_container == null) {
            val intent = SecondActivity.newIntent(this, postId)
            startActivity(intent)
        } else {
            val detail: SecondFragment = SecondFragment.newInstance(postId)

            supportFragmentManager.beginTransaction()
                    .replace(R.id.detail_fragment_container, detail)
                    .commit()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        currentIndex = if (savedInstanceState != null) {
            savedInstanceState.getInt(CURRENT_INDEX, 0)
        } else {
            R.id.nav_news
        }
        setSelection(currentIndex)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CURRENT_INDEX, currentIndex)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == currentIndex) return false
        when (id) {
            R.id.nav_news -> postsFragment.sortPosts(true)
            R.id.nav_date -> postsFragment.sortPosts(false)
            R.id.nav_about -> startActivity(Intent(this, AboutActivity::class.java))
            R.id.nav_policy -> startActivity(Intent(this, PrivacyActivity::class.java))
        }

        currentIndex = id
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun setSelection(id: Int) {
        nav_view.menu.findItem(id).isChecked = true
    }

    companion object {
        private val CURRENT_INDEX = "index"
    }
}
