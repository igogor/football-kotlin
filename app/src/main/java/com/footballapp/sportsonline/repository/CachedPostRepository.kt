package com.footballapp.sportsonline.repository

import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.Maybe
import io.reactivex.Single

interface Repository {
    fun getPosts(page: Int, count: Int): Single<List<Post>>
    fun setPosts(posts: List<Post>)

    fun getPost(postId: Int): Maybe<Post>
    fun setPost(post: Post)
}

class CachedUserRepository(
        private val delegate: NetRepository,
        private val postRepo: Repository
) : Repository by delegate {

    override fun getPosts(page: Int, count: Int): Single<List<Post>> {
        return postRepo.getPosts(page, count).doOnSuccess { posts ->
            posts.forEach { post -> postRepo.setPost(post) }
        }
    }

    override fun getPost(postId: Int): Maybe<Post> {
        return postRepo.getPost(postId)
                .switchIfEmpty(delegate.getPost(postId)
                        .doOnSuccess { postRepo.setPost(it) })
    }
}