package com.footballapp.sportsonline.repository

import com.footballapp.sportsonline.dto.net.Post
import com.footballapp.sportsonline.dto.net.ResultPosts
import com.footballapp.sportsonline.net.retrofit.FootballService
import com.footballapp.sportsonline.net.retrofit.ServiceBuilder
import io.reactivex.Maybe
import io.reactivex.Single
import java.security.Provider
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class NetRepository private constructor(
        private val service: FootballService = ServiceBuilder.buildService(FootballService::class.java)) : Repository {

    private var totalCount = 0
    private var totalPages = 0
    var currentPage = 1
    companion object {

        val INSTANCE: NetRepository by lazy { NetRepository() }
    }

    override fun getPosts(page: Int, count: Int): Single<List<Post>> {
        return service.getPostByPage(currentPage, count)
                .flatMap { result ->
                    totalCount = result.countTotal
                    totalPages = result.pages
                    currentPage++
                    Single.create<ArrayList<Post>> {
                        it.onSuccess(result.posts)
                    }
                }
    }

    override fun getPost(postId: Int): Maybe<Post> {
        return service.getPostDetialsById(postId)
                .flatMap { result ->
                    Maybe.create<Post> { it.onSuccess(result.post!!) }
                }
    }

    override fun setPosts(posts: List<Post>) {

    }

    override fun setPost(post: Post) {

    }
}
