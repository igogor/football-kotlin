package com.footballapp.sportsonline.repository

import android.content.Context
import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class PostRepository private constructor(context: Context) {

    //private val cachedUserRepository = CachedUserRepository(netRepository, RepositoryImpl())
    private var cache:List<Post> = listOf()
    private val netRepository: NetRepository = NetRepository.INSTANCE
    private val dbRepository: DBRepository = DBRepository.getInstance(context)

    companion object {
        private var INSTANCE: PostRepository? = null

        fun getInstance(context: Context): PostRepository {
            if (INSTANCE == null) {
                INSTANCE = PostRepository(context)
            }
            return INSTANCE!!
        }
    }

    fun getPosts(page: Int, count: Int): Observable<List<Post>> {
        return Observable.concatArrayEager(getPostsFromDB(page, count).toObservable(), getPostsFromNet(page, count).toObservable())
    }

    private fun getPostsFromNet(page: Int, count: Int): Single<List<Post>> {
        return if (cache.isNotEmpty()) {
            Single.just(cache)
                    //.mergeWith(netRepository.getPosts())
        } else
            netRepository.getPosts(page, count)
                    .doOnSuccess {
                        /*insertPosts(it) */
                            cache = it
                        }
                    }

    private fun getPostsFromDB(page: Int, count: Int): Single<List<Post>> {
        return dbRepository.getPosts(page, count)
    }

    private fun insertPosts(posts: List<Post>) {
        Single.fromCallable { dbRepository.setPosts(posts) }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

    fun getPost(id: Int): Maybe<Post> {
        return netRepository.getPost(id)
    }
}