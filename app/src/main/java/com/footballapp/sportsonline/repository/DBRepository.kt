package com.footballapp.sportsonline.repository

import android.content.Context
import com.footballapp.sportsonline.db.AppDatabase
import com.footballapp.sportsonline.db.PostDao
import com.footballapp.sportsonline.db.PostWithAuthor
import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.Maybe
import io.reactivex.Single

class DBRepository private constructor(context: Context):Repository {
    private var postsDao: PostDao

    private var appDatabase: AppDatabase? = null
    init {
        appDatabase = AppDatabase.getInstance(context, false)
        postsDao = appDatabase!!.postDao()
    }

    companion object {
        private var INSTANCE: DBRepository? = null
        fun getInstance(context: Context): DBRepository {
            if (INSTANCE == null) {
                INSTANCE = DBRepository(context)
            }
            return INSTANCE!!
        }
    }

    override fun getPosts(page: Int, count: Int): Single<List<Post>> {
        return postsDao.getAll()
    }

    override fun getPost(id: Int): Maybe<Post> {
        return postsDao.getPostById(id)
    }

    override fun setPosts(posts: List<Post>) {
        postsDao.insertAll(*posts.toTypedArray())
    }

    override fun setPost(post: Post) {

    }
}