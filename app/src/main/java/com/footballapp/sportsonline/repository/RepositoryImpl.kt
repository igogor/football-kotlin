package com.footballapp.sportsonline.repository

import com.footballapp.sportsonline.dto.net.Post
import io.reactivex.Maybe
import io.reactivex.Single

class RepositoryImpl:Repository {

    private var _posts:MutableList<Post> = arrayListOf()

    override fun getPosts(page: Int, count: Int): Single<List<Post>> {
        return Single.just(_posts)
    }

    override fun setPosts(posts: List<Post>) {
        _posts = posts.toMutableList()
    }

    override fun getPost(postId: Int): Maybe<Post> {
        return Maybe.just(_posts[postId])
    }

    override fun setPost(post: Post) {
        _posts.add(post)
    }
}